package com.example.labb1;

import java.util.HashMap;

public interface AsyncResponse {
    void processFinish(HashMap<String, Double> output);
}
