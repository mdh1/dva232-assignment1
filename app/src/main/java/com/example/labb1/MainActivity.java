
package com.example.labb1;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Locale;
import android.content.Intent;
import java.util.Currency;

public class MainActivity extends AppCompatActivity {

    View view;
    double input;
    AsyncResponse temp;
    String baseCurrency;
    Spinner spinnerConvertTo;
    Spinner spinnerConvertFrom;

    Map<String, Double> currencyRates;
    ArrayAdapter<CharSequence> adapter;
    HashMap<String, Double> exchangeRates = new HashMap<>();
    static Currency Iso4217Currency = Currency.getInstance(Locale.getDefault());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final EditText field1 = findViewById(R.id.userInput);
        field1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getChosenCurrency(view);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!field1.getText().toString().isEmpty()) {
                    input = Double.parseDouble(field1.getText().toString());
                }
            }
        });

       // setArray();

        adapter = ArrayAdapter.createFromResource(this, R.array.currency_array1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerConvertFrom  = findViewById(R.id.currency_spinner_convertFrom);
        spinnerConvertFrom.setAdapter(adapter);
        spinnerConvertFrom.setOnItemSelectedListener(new spinnerListener());

        spinnerConvertTo = findViewById(R.id.currency_spinner_convertTo);
        spinnerConvertTo.setAdapter(adapter);
        spinnerConvertTo.setOnItemSelectedListener(new spinnerListener());

        temp = new AsyncResponse() {
            @Override
            public void processFinish(HashMap<String, Double> output) {
                currencyRates = new TreeMap<>(output);
                exchangeRates.putAll(currencyRates);

                List<String> list = new ArrayList<>(currencyRates.keySet());

                String[] array = new String[list.size()];
                for (int i = 0; i < list.size(); i++)
                {
                    array[i] = list.get(i);
                }
                ArrayAdapter<CharSequence> dataAdapter = new ArrayAdapter<CharSequence>(MainActivity.this, android.R.layout.simple_spinner_item, array);

                baseCurrency = setBase(exchangeRates, Iso4217Currency.getCurrencyCode());

                adapter = dataAdapter;
                spinnerConvertFrom.setAdapter(adapter);
                spinnerConvertTo.setAdapter(adapter);
                spinnerConvertFrom.setSelection(dataAdapter.getPosition(baseCurrency));
                adapter.notifyDataSetChanged();
            }
        };

        HttpClient httpClient = new HttpClient(temp);
        httpClient.execute();
    }

    public class spinnerListener implements AdapterView.OnItemSelectedListener
    {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long idRow)
        {
            getChosenCurrency(view);
            if (currencyRates != null) {
                baseCurrency = setBase(exchangeRates, spinnerConvertFrom.getItemAtPosition(pos).toString());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0)
        {

        }
    }

    public void openRatesActivity(View view){
        Intent intent = new Intent(this, secondpage.class);
        intent.putExtra("userInput", input);
        intent.putExtra("rates", exchangeRates);
        intent.putExtra("currentCurrency", spinnerConvertFrom.getSelectedItem().toString());
        startActivity(intent);
    }

    private String setBase(Map<String, Double> rates, String newBaseCurrency) {
        if (!rates.containsKey(newBaseCurrency)) {
            Log.v("Base currency", "Currency not found, defaulting to EUR.");
            newBaseCurrency = "EUR";
        }

        double tmp = rates.get(newBaseCurrency);

        Iterator<Map.Entry<String, Double>> it = rates.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Double> pair = it.next();
            pair.setValue(pair.getValue() / tmp);
        }
        Log.v("Base currency", "Base set to " + newBaseCurrency);
        return newBaseCurrency;
    }

    private double currencyManipulate()
    {
        EditText userInput = findViewById(R.id.userInput);

        double userInputFinal = Double.parseDouble(userInput.getText().toString());

        double toDollar = userInputFinal / exchangeRates.get(spinnerConvertFrom.getSelectedItem());
        return toDollar * exchangeRates.get(spinnerConvertTo.getSelectedItem());
    }

    private void getChosenCurrency(View view)
    {
        TextView textview = findViewById(R.id.ResultBox);
        EditText userInput = findViewById(R.id.userInput);

        if (userInput == null || userInput.toString().isEmpty())
        {
            return;
        }
        try{
            double result = currencyManipulate();
            DecimalFormat finalResult = new DecimalFormat("#.00");

            String setResult = (finalResult.format(result));
            textview.setText(setResult);
        }
        catch(Exception e){
            textview.setText("");
        }

    }
}