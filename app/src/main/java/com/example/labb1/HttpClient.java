package com.example.labb1;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class HttpClient extends AsyncTask<HashMap<String, Double>, Void, HashMap<String, Double>>
{
    List<String> keys;
    List<String> values;
    String httpResponse;
    AsyncResponse Delegate;
    HashMap<String, Double> dictionary;

    public HttpClient(AsyncResponse delegate)
    {
        this.Delegate = delegate;
    }

    @Override
    protected HashMap<String, Double> doInBackground(HashMap<String, Double>... Params)
    {
        httpResponse = HttpApiCall();
        keys = getHttpResponseKeys(httpResponse);
        values = getHttpResponseValues(httpResponse);
        return getDictionary();
    }

    @Override
    protected void onPostExecute(HashMap<String, Double> result)
    {
        if(result != null)
        {
            Delegate.processFinish(result);
        }
    }

    public HashMap<String, Double> getDictionary()
    {
        dictionary = new HashMap<>();
        for(int i = 0; i < keys.size()-1; i++)
        {
            dictionary.put(keys.get(i), Double.parseDouble(values.get(i)));
        }
        return dictionary;
    }

    private List<String> getHttpResponseValues(String httpResponse)
    {
        httpResponse = httpResponse.replaceAll("\\{.*\\{", "");
        httpResponse = httpResponse.replaceAll("\"", "");
        httpResponse = httpResponse.replaceAll("\\}", "");
        int colon;
        int lastChar;

        List<String> list = new ArrayList<>(Arrays.asList(httpResponse.split(",")));
        for(int i = 0; i < list.size()-1; i++)
        {
            colon = list.get(i).indexOf(":") + 1;
            lastChar = list.get(i).length();

            list.set(i, list.get(i).substring(colon, lastChar));
        }
        return list; // Denna lista verkar vara tom?

    }

    private List<String> getHttpResponseKeys(String httpResponse) {
        httpResponse = httpResponse.replaceAll("\\{.*\\{", "");
        httpResponse = httpResponse.replaceAll("\"", "");
        httpResponse = httpResponse.replaceAll("\\}", "");

        List<String> list = new ArrayList<>(Arrays.asList(httpResponse.split(",")));
        for(int i = 0; i < list.size()-1; i++)
        {
            list.set(i, list.get(i).substring(0, 3));
        }
        return list; // null object reference. Woot?
    }

    public String HttpApiCall()
    {
        URL url;
        BufferedReader in;
        String response;

        try{
            url = new URL("http://data.fixer.io/api/latest?access_key=4bc593ba5d41c7718114d099fcff7d52");
            in = new BufferedReader(new InputStreamReader((url.openStream())));
            response = in.readLine();
            in.close();
        }catch (Exception e){
            e.printStackTrace();
            response = "total fuckup";
        }
        httpResponse = response;
        return response;
    }
}