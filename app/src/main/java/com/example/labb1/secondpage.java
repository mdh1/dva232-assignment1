package com.example.labb1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.lang.*;
import java.util.Map;
import java.util.TreeMap;


public class secondpage extends AppCompatActivity{

    double input;
    String currentCurrency;
    RecyclerView recyclerView;
    Map<String, Double> exchangeRates;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondpage);
        //AsyncResponse temp;

        Intent intent = getIntent();
        input = intent.getDoubleExtra("userInput", 0);
        exchangeRates = (HashMap<String, Double>) intent.getSerializableExtra("rates");
        currentCurrency = intent.getStringExtra("currentCurrency");

        /*temp = new AsyncResponse() {
            @Override
            public void processFinish(HashMap<String, Double> output) {
                currencyRates = new TreeMap<>(output);
                initRecycleView(currencyRates);
            }
        };*/

        initRecycleView(new TreeMap<String, Double>(exchangeRates));

        //HttpClient httpClient = new HttpClient(temp);
        //httpClient.execute();
    }

    // Used to initialize the entire recycleView, takes map<String, Double> as parameter
    public void initRecycleView(Map<String, Double> map) {
        recyclerView = findViewById(R.id.recView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyAdapter(map));
    }

    // Recycle adapter
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private Map<String, Double> dataset;

        public MyAdapter(Map<String, Double> newDataset) {
            dataset = newDataset;
        }

        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return (new MyViewHolder(v));
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Iterator<Map.Entry<String, Double>> it = dataset.entrySet().iterator();
            DecimalFormat finalResult = new DecimalFormat("#.00");
            int i = 0;
            while (it.hasNext()) {
                Map.Entry<String, Double> pair = it.next();
                if (i == position) {
                    holder.mainText.setText(pair.getKey());
                    holder.subText.setText(finalResult.format(pair.getValue()));
                    return;
                }
                i++;
            }
        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView mainText, subText;
            public MyViewHolder(View v) {
                super(v);
                mainText = v.findViewById(R.id.mainText);
                subText = v.findViewById(R.id.subText);
            }
        }
    }
}